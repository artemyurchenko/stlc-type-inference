{-# LANGUAGE OverloadedStrings #-}

module Main where

import           Control.Arrow           (first, (>>>))
import           Data.ByteString.Builder (hPutBuilder)
import           Data.Function           ((&))
import qualified Data.HashMap.Strict     as M
import           Data.Monoid             ((<>))
import           System.IO               (stdout)

import           Inference               (Inference (..), Rule (..))
import           Lib                     (inferType, parseLTerm, toBSBuilder)
import           LTerm                   (LTerm (..))

main :: IO ()
main = do
  eitherLterm <- parseLTerm <$> getLine
  eitherLterm & either
    (putStrLn . ("ERR: " ++))
    (rename >>> inferType >>> maybe
      (putStrLn "Expression has no type")
      (hPutBuilder stdout . (<> "\n") . toBSBuilder . renameBackInf))

rename :: LTerm -> LTerm
rename = go M.empty
  where go ns (V v)     = maybe (V v) (varFromNum v) (M.lookup v ns)
        go ns (A l1 l2) = A (go ns l1) (go ns l2)
        go ns (L v l)   = go (M.alter (return . maybe 1 (+ 1)) v ns) l

        varFromNum :: String -> Int -> LTerm
        varFromNum v n = V (v ++ "_" ++ show n)

renameBack :: LTerm -> LTerm
renameBack (V v)     = V (demangle v)
renameBack (A l1 l2) = A (renameBack l1) (renameBack l2)
renameBack (L v l)   = L (demangle v) (renameBack l)

demangle :: String -> String
demangle = takeWhile (/= '_')

renameBackInf :: Inference -> Inference
renameBackInf (Inference ctxt lterm ltype rule) = Inference ctxt' lterm' ltype rule'
  where ctxt' = ctxt & M.toList & map (first demangle) & M.fromList
        lterm' = renameBack lterm
        rule' = mapRule renameBackInf rule

mapRule :: (Inference -> Inference) -> Rule -> Rule
mapRule _ Rule1         = Rule1
mapRule f (Rule2 i1 i2) = Rule2 (f i1) (f i2)
mapRule f (Rule3 i)     = Rule3 (f i)
