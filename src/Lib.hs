{-# LANGUAGE LambdaCase #-}

module Lib (parseLTerm, inferType, toBSBuilder, LTerm (..)) where

import qualified Data.ByteString.Char8 as BS (pack)
import           Data.Function         ((&))
import           Text.Printf           (printf)

import           Inference             (inferType)
import           Lexer                 (tokenize)
import           LTerm                 (LTerm (..))
import           LTermParser
import           Utils                 (toBSBuilder, (|>))


parseLTerm :: String -> Either String LTerm
parseLTerm s = s & BS.pack & tokenize
          |> runP ltermP
          >>= \case Just (term, []) -> Right term
                    Just (term, ts) -> Left $ printf "'%s' is left unparsed (parsed: '%s')" (show ts) (show term)
                    Nothing -> Left "parsing failed"
