{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TypeFamilies    #-}
{-# LANGUAGE ViewPatterns    #-}

module Stream (Stream(..), pattern (:<), pattern Nil) where

import qualified Data.List as L (uncons)
import qualified Data.ByteString.Char8 as BS

class Stream s where
  type Token s :: *
  empty :: s
  cons :: Token s -> s -> s
  uncons :: s -> Maybe (Token s, s)

{-# COMPLETE Nil, (:<) :: Stream #-}

infixr 5 :<
pattern (:<) :: Stream s => Token s -> s -> s
pattern b :< bs <- (uncons -> Just (b, bs))
  where b :< bs = cons b bs

pattern Nil :: Stream s => s
pattern Nil <- (uncons -> Nothing)
  where Nil = empty

instance Stream [a] where
  type Token [a] = a
  empty = []
  cons = (:)
  uncons = L.uncons

instance Stream BS.ByteString where
  type Token BS.ByteString = Char
  empty = BS.empty
  cons = BS.cons
  uncons = BS.uncons
