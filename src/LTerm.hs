{-# LANGUAGE OverloadedStrings #-}

module LTerm (LTerm (..), LType (..)) where

import           Data.ByteString.Builder (string7)
import           Data.Monoid             ((<>))

import           Utils                   (ToBSBuilder (..))

data LTerm = L String LTerm
           | A LTerm LTerm
           | V String
           deriving (Show, Eq)

data LType = T String | LType :-> LType deriving (Show, Eq)
infixr 9 :->

data STLTerm = STLTerm LTerm LType

instance ToBSBuilder LTerm where
  toBSBuilder (L v t)   = mconcat ["(\\", string7 v, ".", toBSBuilder t, ")"]
  toBSBuilder (A t1 t2) = mconcat ["(", toBSBuilder t1, " ", toBSBuilder t2, ")"]
  toBSBuilder (V s)     = string7 s

instance ToBSBuilder LType where
  toBSBuilder (T t)       = "t" <> string7 t
  toBSBuilder (t1 :-> t2) = mconcat ["(", toBSBuilder t1, " -> ", toBSBuilder t2, ")"]
